<?php
require 'database.php';
session_start();
if (isset($_SESSION['user_id'])) {
	echo ("Hello " . $_SESSION['user_id'] . "!");
} else {
	echo ("Hello guest user!");
}
?>

<!DOCTYPE html>
<html>
	<head>
		<title>DAILY NEWS SITE</title>
	</head>

	<body>
		<form id='loginsignup' action='homepage.html' method='get'>
				<br>
				<h1>DAILY NEWS SITE</h1>
   			 <input type="submit" value="LOGIN/SIGNUP" 
         		name="Submit" id="frm1_submit" />
		</form>

		<form id='logout' action='logout.php' method='get'>
		     <input type="submit" value="LOGOUT" 
         		name="Submit" id="frm2_submit" />
        </form>
        <div class="homepage">
	        <div class="welcome">
	            <h2>Latest news</h2>
	            <p>Welcome to the daily news site. <em>UPDATED DAILY!</em></p>
	        </div>

		<form id='newpost' action='newpost.html' method='get'>
		     <input type="submit" value="NEW POST" 
         		name="Submit" id="frm3_submit" />
        </form>
	    </div>

	<div id="funny">
		<p id="tags">FUNNY</p>
	<?php
		//DISPLAY ALL FUNNY TAG STORIES
		require 'database.php';

		$tags = "funny";

		$stmt = $mysqli->prepare("select post_id, username, title, content, tags from posts where tags=?");
		if(!$stmt){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}

		$stmt->bind_param('s', $tags);

		$stmt->execute();

		$stmt->bind_result($post_id1, $username, $title, $content, $tags);
		
		echo "<ul>\n";
		while($stmt->fetch()){
			printf("\t<li> Story: %s <a href=%s target='_blank'>Link</a> \t Posted by: <i>%s</i>
				<form name='operations' method='POST' action='operations.php'>
				<input type='hidden' name='post_id' value='$post_id1'>
				<input type='hidden' name='username' value='$username'>
				<input type='radio' name='operation' value='View Comments'>View Comments
				<input type='radio' name='operation' value='Edit'>Edit Post
				<input type='radio' name='operation' value='Delete'>Delete Post
				<input type='submit' value='Go!'/>
				</form></li> \n",
				htmlspecialchars($content),
				htmlspecialchars($title),
				htmlspecialchars($username)
				);
		}
		echo "</ul>\n";

		$stmt->close();
		?>
	</div>

	<div id="trending">
		<p id="tags">TRENDING</p>
	<?php
		//DISPLAY ALL TRENDING TAG STORIES
		require 'database.php';

		$tags = "trending";

		$stmt = $mysqli->prepare("select post_id, username, title, content, tags from posts where tags=?");
		if(!$stmt){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}

		$stmt->bind_param('s', $tags);

		$stmt->execute();

		$stmt->bind_result($post_id2, $username, $title, $content, $tags);
		
		echo "<ul>\n";
		while($stmt->fetch()){
			printf("\t<li> Story: %s <a href=%s target='_blank'>Link</a> \t Posted by: <i>%s</i>
				<form name='operations' method='POST' action='operations.php'>
				<input type='hidden' name='post_id' value='$post_id2'>
				<input type='hidden' name='username' value='$username'>
				<input type='radio' name='operation' value='View Comments'>View Comments
				<input type='radio' name='operation' value='Edit'>Edit Post
				<input type='radio' name='operation' value='Delete'>Delete Post
				<input type='submit' value='Go!'/>
				</form></li> \n",
				htmlspecialchars($content),
				htmlspecialchars($title),
				htmlspecialchars($username)
				);
		}
		echo "</ul>\n";

		$stmt->close();
		?>
	</div>

	<div id="world">
		<p id="tags">WORLD</p>
	<?php
		//DISPLAY ALL WORLD TAG STORIES
		require 'database.php';

		$tags = "world";

		$stmt = $mysqli->prepare("select post_id, username, title, content, tags from posts where tags=?");
		if(!$stmt){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}

		$stmt->bind_param('s', $tags);

		$stmt->execute();

		$stmt->bind_result($post_id3, $username, $title, $content, $tags);
		
		echo "<ul>\n";
		while($stmt->fetch()){
			printf("\t<li> Story: %s <a href=%s target='_blank'>Link</a> \t Posted by: <i>%s</i>
				<form name='operations' method='POST' action='operations.php'>
				<input type='hidden' name='post_id' value='$post_id3'>
				<input type='hidden' name='username' value='$username'>
				<input type='radio' name='operation' value='View Comments'>View Comments
				<input type='radio' name='operation' value='Edit'>Edit Post
				<input type='radio' name='operation' value='Delete'>Delete Post
				<input type='submit' value='Go!'/>
				</form></li> \n",
				htmlspecialchars($content),
				htmlspecialchars($title),
				htmlspecialchars($username)
				);
		}
		echo "</ul>\n";

		$stmt->close();
		?>
	</div>

	<div id="us">
		<p id="tags">U.S.</p>
	<?php
		//DISPLAY ALL U.S. TAG STORIES
		require 'database.php';

		$tags = "us";

		$stmt = $mysqli->prepare("select post_id, username, title, content, tags from posts where tags=?");
		if(!$stmt){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}

		$stmt->bind_param('s', $tags);

		$stmt->execute();

		$stmt->bind_result($post_id4, $username, $title, $content, $tags);
		
		echo "<ul>\n";
		while($stmt->fetch()){
			printf("\t<li> Story: %s <a href=%s target='_blank'>Link</a> \t Posted by: <i>%s</i>
				<form name='operations' method='POST' action='operations.php'>
				<input type='hidden' name='post_id' value='$post_id4'>
				<input type='hidden' name='username' value='$username'>
				<input type='radio' name='operation' value='View Comments'>View Comments
				<input type='radio' name='operation' value='Edit'>Edit Post
				<input type='radio' name='operation' value='Delete'>Delete Post
				<input type='submit' value='Go!'/>
				</form></li> \n",
				htmlspecialchars($content),
				htmlspecialchars($title),
				htmlspecialchars($username)
				);
		}
		echo "</ul>\n";

		$stmt->close();
		?>
	</div>

	<div id="politics">
		<p id="tags">POLITICS</p>
	<?php
		//DISPLAY ALL POLITICS TAG STORIES
		require 'database.php';

		$tags = "politics";

		$stmt = $mysqli->prepare("select post_id, username, title, content, tags from posts where tags=?");
		if(!$stmt){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}

		$stmt->bind_param('s', $tags);

		$stmt->execute();

		$stmt->bind_result($post_id5, $username, $title, $content, $tags);
		
		echo "<ul>\n";
		while($stmt->fetch()){
			printf("\t<li> Story: %s <a href=%s target='_blank'>Link</a> \t Posted by: <i>%s</i>
				<form name='operations' method='POST' action='operations.php'>
				<input type='hidden' name='post_id' value='$post_id5'>
				<input type='hidden' name='username' value='$username'>
				<input type='radio' name='operation' value='View Comments'>View Comments
				<input type='radio' name='operation' value='Edit'>Edit Post
				<input type='radio' name='operation' value='Delete'>Delete Post
				<input type='submit' value='Go!'/>
				</form></li> \n",
				htmlspecialchars($content),
				htmlspecialchars($title),
				htmlspecialchars($username)
				);
		}
		echo "</ul>\n";

		$stmt->close();
		?>
	</div>

	<div id="sports">
		<p id="tags">SPORTS</p>
	<?php
		//DISPLAY ALL SPORTS TAG STORIES
		require 'database.php';

		$tags = "sports";

		$stmt = $mysqli->prepare("select post_id, username, title, content, tags from posts where tags=?");
		if(!$stmt){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}

		$stmt->bind_param('s', $tags);

		$stmt->execute();

		$stmt->bind_result($post_id6, $username, $title, $content, $tags);
		
		echo "<ul>\n";
		while($stmt->fetch()){
			printf("\t<li> Story: %s <a href=%s target='_blank'>Link</a> \t Posted by: <i>%s</i>
				<form name='operations' method='POST' action='operations.php'>
				<input type='hidden' name='post_id' value='$post_id6'>
				<input type='hidden' name='username' value='$username'>
				<input type='radio' name='operation' value='View Comments'>View Comments
				<input type='radio' name='operation' value='Edit'>Edit Post
				<input type='radio' name='operation' value='Delete'>Delete Post
				<input type='submit' value='Go!'/>
				</form></li> \n",
				htmlspecialchars($content),
				htmlspecialchars($title),
				htmlspecialchars($username)
				);
		}
		echo "</ul>\n";

		$stmt->close();
		?>
	</div>

	<div id="arts">
				<p id="tags">ARTS</p>
	<?php
		//DISPLAY ALL ARTS TAG STORIES
		require 'database.php';

		$tags = "arts";

		$stmt = $mysqli->prepare("select post_id, username, title, content, tags from posts where tags=?");
		if(!$stmt){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}

		$stmt->bind_param('s', $tags);

		$stmt->execute();

		$stmt->bind_result($post_id7, $username, $title, $content, $tags);
		
		echo "<ul>\n";
		while($stmt->fetch()){
			printf("\t<li> Story: %s <a href=%s target='_blank'>Link</a> \t Posted by: <i>%s</i>
				<form name='operations' method='POST' action='operations.php'>
				<input type='hidden' name='post_id' value='$post_id7'>
				<input type='hidden' name='username' value='$username'>
				<input type='radio' name='operation' value='View Comments'>View Comments
				<input type='radio' name='operation' value='Edit'>Edit Post
				<input type='radio' name='operation' value='Delete'>Delete Post
				<input type='submit' value='Go!'/>
				</form></li> \n",
				htmlspecialchars($content),
				htmlspecialchars($title),
				htmlspecialchars($username)
				);
		}
		echo "</ul>\n";

		$stmt->close();
		?>
	</div>

	<div id="health">
				<p id="tags">HEALTH</p>
	<?php
		//DISPLAY ALL HEALTH TAG STORIES
		require 'database.php';

		$tags = "health";

		$stmt = $mysqli->prepare("select post_id, username, title, content, tags from posts where tags=?");
		if(!$stmt){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}

		$stmt->bind_param('s', $tags);

		$stmt->execute();

		$stmt->bind_result($post_id8, $username, $title, $content, $tags);
		
		echo "<ul>\n";
		while($stmt->fetch()){
			printf("\t<li> Story: %s <a href=%s target='_blank'>Link</a> \t Posted by: <i>%s</i>
				<form name='operations' method='POST' action='operations.php'>
				<input type='hidden' name='post_id' value='$post_id8'>
				<input type='hidden' name='username' value='$username'>
				<input type='radio' name='operation' value='View Comments'>View Comments
				<input type='radio' name='operation' value='Edit'>Edit Post
				<input type='radio' name='operation' value='Delete'>Delete Post
				<input type='submit' value='Go!'/>
				</form></li> \n",
				htmlspecialchars($content),
				htmlspecialchars($title),
				htmlspecialchars($username)
				);
		}
		echo "</ul>\n";

		$stmt->close();
		?>
	</div>

	<div id="science">
				<p id="tags">SCIENCE</p>
	<?php
		//DISPLAY ALL SCIENCE TAG STORIES
		require 'database.php';

		$tags = "science";

		$stmt = $mysqli->prepare("select post_id, username, title, content, tags from posts where tags=?");
		if(!$stmt){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}

		$stmt->bind_param('s', $tags);

		$stmt->execute();

		$stmt->bind_result($post_id9, $username, $title, $content, $tags);
		
		echo "<ul>\n";
		while($stmt->fetch()){
			printf("\t<li> Story: %s <a href=%s target='_blank'>Link</a> \t Posted by: <i>%s</i>
				<form name='operations' method='POST' action='operations.php'>
				<input type='hidden' name='post_id' value='$post_id9'>
				<input type='hidden' name='username' value='$username'>
				<input type='radio' name='operation' value='View Comments'>View Comments
				<input type='radio' name='operation' value='Edit'>Edit Post
				<input type='radio' name='operation' value='Delete'>Delete Post
				<input type='submit' value='Go!'/>
				</form></li> \n",
				htmlspecialchars($content),
				htmlspecialchars($title),
				htmlspecialchars($username)
				);
		}
		echo "</ul>\n";

		$stmt->close();
		?>
	</div>

	<div class="footer">
	    jennyliu330.com - all rights reserved.
	</div>

	</body>
</html>
