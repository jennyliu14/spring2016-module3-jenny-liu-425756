<?php
session_start();

require 'database.php';

if(isset($_SESSION['user_id'])) {

	$username = $_SESSION['user_id'];
	$post_id= $_POST['post_id'];
	$comments = $_POST['comment'];

	$stmt = $mysqli->prepare("insert into comments (id, username, comments) values (?, ?, ?) ");

	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}

	$stmt->bind_param('dss', $post_id, $username, $comments);

	$stmt->execute();

	$stmt->close();

	//Back to Comments
	echo ("Comment added! <form name='backtoComments' method='POST' action='operations.php'>
		<input type='hidden' name='username' value='$username'>
		<input type='hidden' name='post_id' value='$post_id'>
		<input type='hidden' name='operation' value='View Comments'>
		<input type='submit' value='Back to Comments'/>
		</form>");
} else {
	header("Location: homepage.html");
}

?>