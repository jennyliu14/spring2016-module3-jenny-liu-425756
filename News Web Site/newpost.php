<?php
require 'database.php';
session_start();

if(isset($_SESSION['user_id'])) {
	$username = trim($_SESSION['user_id']);
	$title = $_POST['title'];
	$content = $_POST['content'];
	$tags = $_POST['tags'];

	$stmt = $mysqli->prepare("insert into posts (username, title, content, tags) values (?, ?, ?, ?)");
	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}

	$stmt->bind_param('ssss', $username, $title, $content, $tags);
	 
	$stmt->execute();
	 
	$stmt->close();

}

    header("Location: newssite.php");
exit;
?>