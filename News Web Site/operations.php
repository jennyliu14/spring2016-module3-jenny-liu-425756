<?php
session_start();
require 'database.php';

//HOME BUTTON
echo ('<form name="backHome" method="POST" action="newssite.php" id="backhome">
	<input type="submit" value="Home"/>
	</form><br>');

if(isset($_SESSION['user_id'])) {

}
	$username = $_POST['username']; //set to this post's username
	$operation = $_POST['operation'];
	$post_id = $_POST['post_id'];

	//VIEW COMMENTS
	if ($operation == 'View Comments') { 
		
		$stmt = $mysqli->prepare("select comments.comment_id, comments.username, comments.comments from comments where (comments.id=?)");
		if(!$stmt){
			printf("Query Prep Failed: %s\n", $mysqli->error);
			exit;
		}

		$stmt->bind_param('s', $post_id);

		$stmt->execute();

		$stmt->bind_result($id, $username, $comments);
		
		echo "</ul>\n";

	//SHOW COMMENTS AND ALSO OPTIONS TO EDIT/DELETE
		echo ("<div><h2>Comments</h2></div>");
		while($stmt->fetch()){
			printf("\t<li> Comment: %s \t posted by: <i>%s</i> comment ID: <i>%s</i>
				\n",
				htmlspecialchars($comments),
				htmlspecialchars($username),
				htmlspecialchars($id)
				);
		}
		echo "<div><ul>\n";

			//EDIT/DELETE COMMENT BOX
		echo ("<div><h3>Edit/Delete Comments</h3>
			<form name='commentOps' method='POST' action='editcomment.php'>
				<input type='hidden' name='post_id' value='$post_id'>
				<input type='hidden' name='operation' value='$operation'>
				<input type='text' name='commID'>Which comment would you like to edit/delete? (Input comment ID)<br>
				<input type='text' name='editedComment'>Edited comment<br>
				<input type='radio' name='commentOperation' value='Edit'>Edit Comment
				<input type='radio' name='commentOperation' value='Delete'>Delete Comment
				<input type='submit' value='Edit/Delete comment'/>
			</form></div>");

		$stmt->close();	
	//COMMENT BOX

	echo ("<div><form name='comment' method='POST' action='comments.php'>
		<h3>Add a comment:</h3>
		<input type='text' name='comment'><br>
		<input type='hidden' name='post_id' value='$post_id'>
		<input type='submit' value='Submit'/>
		</form></div>");
	}

	//EDIT POST
	else if ($_POST['operation'] ==  'Edit') {
		if ($_SESSION['user_id'] == $username) { //check if current user and user who posted are same

			//SHOW OLD POST DATA
			$stmt = $mysqli->prepare("select post_id, title, content, username from posts where post_id=?");
			if(!$stmt){
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit;
			}

			$stmt->bind_param('s', $post_id);

			$stmt->execute();

			$stmt->bind_result($post_original, $title_original, $content_original, $user_original);

			echo "<ul>\n";
			while($stmt->fetch()){
				printf("\t<div><li> Original Post: %s Original Link: <a href=%s target='_blank'>Link</a> \t with post ID: %s posted by: <i>%s</i>
					</li></div> \n",
					htmlspecialchars($content_original),
					htmlspecialchars($title_original),
					htmlspecialchars($post_original),
					htmlspecialchars($user_original)
					);
			}
			echo "</ul>\n";	

			$stmt->close();

			//FORM FOR EDITING POST
			echo ("<div>
				<form name='Edit Post' method='POST' action='editpost.php'>
				<input type='hidden' name='post_id' value='$post_id'>
				Edit link: <input type='text' name='editlink'><br>
				Edit content: <input type='text' name='editpost'><br>
				Edit category:
				<input type='radio' name='editcategory' value='Funny'>Funny
				<input type='radio' name='editcategory' value='Trending'>Trending
				<input type='radio' name='editcategory' value='World'>World
				<input type='radio' name='editcategory' value='US'>U.S.
				<input type='radio' name='editcategory' value='Politics'>Politics
				<input type='radio' name='editcategory' value='Sports'>Sports
				<input type='radio' name='editcategory' value='Arts'>Arts
				<input type='radio' name='editcategory' value='Health'>Health
				<input type='radio' name='editcategory' value='Science'>Science<br>
				<input type='submit' value='Edit Post'/>
				</form></div>");

		} else {
			Header("Location: error.html");//session user and user who posted not the same
		}

	}

//DELETE POST
	else if ($_POST['operation'] == 'Delete') {
		if ($_SESSION['user_id'] == $username) { //check if current user and user who posted link are same
			$stmt = $mysqli->prepare("delete from posts where post_id=?");

			$stmt->bind_param('s', $post_id);

			$stmt->execute();

			$stmt->close();

			Header ("Location: newssite.php");
		} else {
			Header("Location: error.html"); //session user and user who posted not the same
		}
	}


?>
<!DOCTYPE html>
<html>
	<head><title>COMMENTS</title></head>
	<body>
	</body>
</html>
