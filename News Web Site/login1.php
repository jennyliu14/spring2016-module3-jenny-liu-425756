<?php
// This is a *good* example of how you can implement password-based user authentication in your web application.

require 'database.php';

// Use a prepared statement
$stmt = $mysqli->prepare("SELECT COUNT(*), username, password FROM accounts WHERE username=?");
 
// Bind the parameter
$stmt->bind_param('s', $user);
$user = $_POST['username'];
$stmt->execute();
 
// Bind the results
$stmt->bind_result($cnt, $user_id, $pwd_hash);
$stmt->fetch();
 
$pwd_guess = $_POST['password'];
// Compare the submitted password to the actual password hash
if( $cnt == 1 && crypt($pwd_guess, $pwd_hash)==$pwd_hash){
	// Login succeeded!
	session_start();
	$_SESSION['user_id'] = $user_id;
	$_SESSION['token'] = substr(md5(rand()), 0, 10); // generate a 10-character random string
	header("Location: newssite.php");
	// Redirect to your target page
}else{

	echo "Login Failed: Incorrect Username and Password";
	// Login failed; redirect back to the login screen
}
?>