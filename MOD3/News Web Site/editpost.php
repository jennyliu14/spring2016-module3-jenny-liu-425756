<?php
session_start();
require 'database.php';

$post_id = $_POST['post_id'];
$newlink = "";
$newtitle = "";
$newcategory = "";

if (isset($_POST['editlink'])) {	
	$newlink = $_POST['editlink'];
}
if (isset($_POST['editpost'])) {
	$newtitle = $_POST['editpost'];
}
if (isset($_POST['editcategory'])) {
	$newcategory = $_POST['editcategory'];
}

$stmt = $mysqli->prepare("update posts set title=?, content=?, tags=? where posts.post_id=?");
if(!$stmt){
	printf("Query Prep Failed: %s\n", $mysqli->error);
	exit;
}

$stmt->bind_param('ssss', $newlink, $newtitle, $newcategory, $post_id);

$stmt->execute();

$stmt->close();

Header ("Location: newssite.php");
?>