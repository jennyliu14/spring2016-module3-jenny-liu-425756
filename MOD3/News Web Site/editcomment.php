<?php
session_start();

require 'database.php';


	$commentID = $_POST['commID'];
	$operation = $_POST['commentOperation'];

	if(isset($_POST['editedComment'])) {
		$editToComment = $_POST['editedComment'];
	} else {
		$editToComment = "";
	}

		//DELETE COMMENT
	if (isset($_SESSION['user_id'])) {
		if ($operation == 'Delete') {
			$stmt = $mysqli->prepare("delete from comments where comment_id=? AND username=?");

			if(!$stmt){
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit;
			}

			$stmt->bind_param('ss', $commentID, $_SESSION['user_id']);

			$stmt->execute();

			$stmt->close();


		//HOME BUTTON
			echo ('<form name="backHome" method="POST" action="newssite.php" id="backhome">
				<input type="submit" value="Home"/>
				</form><br>');			
		} 
		//EDIT COMMENT
		else if ($operation == 'Edit') {
			$stmt = $mysqli->prepare("update comments set comments=? where comments.comment_id=? AND comments.username=?");
			if(!$stmt){
				printf("Query Prep Failed: %s\n", $mysqli->error);
				exit;
			}

			$stmt->bind_param('sss', $editToComment, $commentID, $_SESSION['user_id']);

			$stmt->execute();

			$stmt->close();

		//HOME BUTTON
			echo ('<form name="backHome" method="POST" action="newssite.php" id="backhome">
				<input type="submit" value="Home"/>
				</form><br>');	
		}
	} else {
		header("Location: error.html");//If session user is not same as comment poster
	}

	?>